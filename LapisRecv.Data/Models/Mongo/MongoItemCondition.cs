﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LapisRecv.Data.Models.Mongo
{
    public class MongoItemCondition : MongoDocumentBase
    {
        [BsonRequired]
        public string Condition { get; set; }

        [BsonRequired]
        public bool RequireInspection { get; set; }

        [BsonRequired]
        public bool PrimaryInventory { get; set; }
    }
}
