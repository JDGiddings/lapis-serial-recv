﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LapisRecv.Domain.Models
{
    public class Product
    {
        public string Id { get; set; }

        public string UPC { get; set; }

        public string Brand { get; set; }

        public string Model { get; set; }

        public string ItemDescription { get; set; }

        public string SerialType { get; set; }

        public DateTime CreateDate { get; set; }

        public string SerialPattern { get; set; }
    }
}
