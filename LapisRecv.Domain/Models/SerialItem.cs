﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LapisRecv.Domain.Models
{
    public class SerialItem
    {
        public string Id { get; set; }

        public string SerialNumber { get; set; }

        public Product Product { get; set; }

        public DateTime ReceiveDate { get; set; }

        public ItemCondition ItemCondition { get; set; }

        public ItemCost Cost { get; set; }

        public ItemNotes[] Notes { get; set; }

        public Inspection[] Inspections { get; set; }

        public bool IsScannedOut { get; set; }

        public string OrderNumber { get; set; }

        public DateTime? ScanOutDate { get; set; }
    }
}
