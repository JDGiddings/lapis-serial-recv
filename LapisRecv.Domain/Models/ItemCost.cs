﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LapisRecv.Domain.Models
{
    public class ItemCost
    {
        [BsonRequired, Required]
        public double Amount { get; set; }

        [BsonRequired, Required]
        public string Currency { get; set; }
    }
}
