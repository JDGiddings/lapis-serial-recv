﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LapisRecv.Domain.Models
{
    public class Inspection
    {
        public bool Passed { get; set; }
        public string Inspector { get; set; }
        public DateTime InspectionDate { get; set; }
    }
}
