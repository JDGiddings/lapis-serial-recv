﻿using LapisRecv.Logic.Managers;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LapisRecv.Logic
{
    public static class Startup
    {
        public static void Register(IServiceCollection services)
        {
            services.AddTransient<ISerialRecvManager, SerialRecvManager>();

            Data.Startup.Register(services);
        }
    }
}
