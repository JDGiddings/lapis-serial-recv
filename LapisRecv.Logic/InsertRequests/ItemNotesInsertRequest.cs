﻿using LapisRecv.Domain.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LapisRecv.Logic.InsertRequests
{
    public class ItemNotesInsertRequest : InsertRequestBase
    {
        [Required]
        public string Author { get; set; }

        [Required]
        public string Notes { get; set; }

        [NotMapped]
        public override bool isValid { get => true; }

        public ItemNotes ToMongoItemNotes() =>
            new ItemNotes()
            {
                Author = Author,
                Notes = Notes,
                WrittenDate = DateTime.Now
            };
    }
}
