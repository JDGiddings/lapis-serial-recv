﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LapisRecv.Logic.InsertRequests
{
    public class SerialRecvInsertRequest : InsertRequestBase
    {
        [Required]
        public string Purchaser { get; set; }

        [Required]
        public string Vehicle { get; set; }

        [Required]
        public SerialItemInsertRequest[] Items { get; set; }

        [NotMapped]
        public override bool isValid { get => !Items.Select(x => x.isValid).Contains(false); }
    }
}
