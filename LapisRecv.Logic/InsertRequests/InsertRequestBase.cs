﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LapisRecv.Logic.InsertRequests
{
    public abstract class InsertRequestBase
    {
        public abstract bool isValid { get; }
    }
}
