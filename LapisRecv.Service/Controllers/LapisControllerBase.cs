﻿using Microsoft.AspNetCore.Mvc;
using LapisRecv.Logic.InsertRequests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace LapisRecv.Service.Controllers
{
    public abstract class LapisControllerBase : ControllerBase
    {
        public async Task<ActionResult> ExecuteRequestAsync<TIn, TResponse>(
           Func<TIn, Task<TResponse>> exec,
           TIn model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await exec(model);

            if (result is null)
            {
                return NotFound(model);
            }

            return Ok(result); 
        }

        public async Task<ActionResult> ExecuteInsertRequestAsync<TIn, TResponse>(
            Func<TIn, Task<TResponse>> exec,
            TIn model)
            where TIn : InsertRequestBase
        {
            if (!model.isValid)
            {
                //If the model fails validation, return 422
                //This should never happen, as data validation is done live for the user
                //by ErrorCheckController. However, if the user attempts to bypasss 
                //ErrorCheckController's warnings (via hitting the API endpoint directly
                //using Postman or similar) this will stop them
                return UnprocessableEntity(model);
            }

            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await exec(model);
            return Created("", result);
        }

        public async Task<ActionResult> GetResultsAsync<T>(Func<Task<T>> exec)
        {
            //Doing => Ok(await exec()); doesn't work for some reason
            var result = await exec();
            return Ok(result);
        }
            
    }
}
